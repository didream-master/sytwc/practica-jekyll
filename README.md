# Práctica Jekyll
El propósito del presente proyecto es la creación de una plantilla, usando Jekyll, con la siguiente estructura:

| Estructura |
|------|
| ![practica-prototipo.png](practica-prototipo.png) |

Para ello, partiendo del proyecto del repositorio https://github.com/jekyll/example, se ha creado una página con un listado de algunos grados de la ULL.

## Implementación
### Creación de la colección de cursos
Para la creación de la colección de los cursos se ha añadido a [_config.yml](_config.yml):
```yml
collections:
  courses:
    output: true
```

Además, para mostar una página con la información en detalle del curso se ha creado el layout [_layouts/course.html](_layouts/course.html), y especificado que por defecto los elementos de dicha colección usen este layout. Para ello se ha añdido a [_config.yml](_config.yml):
```yml
defaults:
  - scope:
      path: ""
      type: "courses"
    values:
      layout: course
```

De igual forma, se ha borrado el código referente a los posts puesto que no es útil para el sitio web.


### Cambio de diseño
Para la implementación de un diseño acorde al prototipo propuesto, se han dejado de usar los estilos css del proyecto base y se ha añadido Materialize. Para ello se han incorporando los estilos css y el js respectivos en [_includes/head.html](_includes/head.html)
```html
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
```

y, con el uso de Materialize, adaptado el header y footer de la página; en [_includes/header.html](_includes/header.html) y [_includes/footer.html](_includes/footer.html) respectivamente, al prototipo comentado. De igual forma, se ha adaptado el [index.html](index.html) del proyecto para mostrar el listado de grados.

### Adición de nueva página
Se ha añadido una página muy básica al sitio web llamado ofertas.

Con el fin de agrupar las páginas del sitio web en un directorio, se ha añadido lo siguiente a [_config.yml](_config.yml):
```yml
include: ['_pages']
```
De esta forma, las páginas pueden estar localizadas en [_pages](_pages).

### Despliegue del sitio web
Se ha desplegado el sitio web mediante [Vercel](https://vercel.com/) (https://sytwc-practica-jekyll.didream.vercel.app/) y Github Pages (https://didream.github.io/sytwc-practica-jekyll/). Con Github Pages a diferencia que en Vercel fue necesario especificar el `baseUrl` en [_config.yml](_config.yml) con el nombre del repositorio de Github.