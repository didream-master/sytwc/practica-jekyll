---
title: Ingeniería Informática
shortDescription: El Grado en Ingeniería Informática ha obtenido en 2019 el sello de calidad Euro-Inf, con validez hasta 2025...
imageUrl: https://cdn.pixabay.com/photo/2016/11/18/18/37/programming-1836330_960_720.png
---

El Grado en Ingeniería Informática ha obtenido en 2019 el sello de calidad Euro-Inf, con validez hasta 2025. Esta distinción acredita que se cumplen los estándares internacionales de calidad establecidos por la European Quality Assurance Network for Informatics Education (EQANIE), en el ámbito de la Informática. El sello Euro-Inf está reconocido por empleadores de toda Europa y facilita la movilidad académica y profesional de los graduados y graduadas. Puede encontrar más información acerca de los beneficios que aporta el sello Euro-Inf en la web de ANECA y en la de EQANIE .

El grado en Ingeniería Informática permite desarrollar profesiones relacionadas con inteligencia artificial, robótica, diseño multimedia, gráficos y animación, programación de juegos, realidad virtual, aplicaciones industriales, seguridad en redes, etc., en áreas tan diversas como de software y servicios, productos y sistemas, telecomunicaciones, etc.

El objetivo principal de este grado es la formación científica, tecnológica y socioeconómica en el ámbito de la Informática, que conduzca a una adecuada preparación para el ejercicio profesional en el desarrollo y aplicación de las tecnologías de la información y las comunicaciones (TIC).

Se pretende formar una persona que sea experta en tecnología del software, en arquitectura y tecnología de los computadores, en tecnología de las redes de computadores y en equipos electrónicos.